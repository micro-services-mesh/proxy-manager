package api

import (
	"proxymanager/app"
	"strings"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// router is a type alias for a map where the keys are strings and the values are pointers to mux.Router.
// This type alias is used to manage multiple routers in the proxy-manager API.
// It is used to store the base routes for the API.
//
// Example:
//
//	r := make(router)
//	r["ApiRoot"] = mux.NewRouter()
//	r["Services"] = r["ApiRoot"].PathPrefix("/services").Subrouter()
//	r["Users"] = r["ApiRoot"].PathPrefix("/users").Subrouter()
type router = map[string]*mux.Router

// API represents the main structure for the API layer of the proxy manager.
// It contains references to the application instance, base routes, base path,
// root router, and a logger for logging purposes.
//
// Fields:
// - App: A reference to the main application instance.
// - BaseRoutes: A map of base routes for the API.
// - BasePath: The base path for the API.
// - RootRouter: The root router for the API.
// - Logger: A logger for logging purposes.
//
// Example:
//
//	api := &API{
//		App:        app,
//		BaseRoutes: make(router),
//		BasePath:   "/api/v1",
//		RootRouter: mux.NewRouter(),
//		Logger:     logrus.New(),
//	}
//
//	api.BaseRoutes["ApiRoot"] = api.RootRouter.PathPrefix(api.BasePath).Subrouter()
//	api.BaseRoutes["Services"] = api.BaseRoutes["ApiRoot"].PathPrefix("/services").Subrouter()
//	api.BaseRoutes["Users"] = api.BaseRoutes["ApiRoot"].PathPrefix("/users").Subrouter()
//
//	api.Logger.WithField("basePath", api.BasePath).Infoln("Api configured with")
type API struct {
	App        *app.App
	BaseRoutes router
	basePath   string
	rootRouter *mux.Router
	logger     logrus.FieldLogger
}

// Init initializes a new API instance with the provided options.
// It sets up the base routes and services for the API, and logs the configuration details.
//
// Parameters:
//
//	options - A variadic list of Options to configure the API instance.
//
// Returns:
//
//	A pointer to the initialized API instance.
//
// Example:
//
//	api := Init(WithBasePath("/api/v1"), WithLogger(logger))
//
//	api.BaseRoutes["ApiRoot"] = api.RootRouter.PathPrefix(api.BasePath).Subrouter()
//	api.BaseRoutes["Services"] = api.BaseRoutes["ApiRoot"].PathPrefix("/services").Subrouter()
//	api.BaseRoutes["Users"] = api.BaseRoutes["ApiRoot"].PathPrefix("/users").Subrouter()
//
//	api.Logger.WithField("basePath", api.BasePath).Infoln("Api configured with")
//	api.Logger.WithField("api", "Services,Users").Infoln("Initialized api")
//
//	return api
//
// Errors:
//   - Sets an application error in the context if there is an issue creating the file, reading the request body,
//     writing the encrypted data to the file, or writing the response.
//
// Success:
//   - Writes a success message to the response if the proxy configuration is registered successfully.
//
// See Also:
// - Options
// - WithBasePath
// - WithLogger
// - WithRouter
// - WithManagers
// - WithConfigStorage
// - WithEncryptionSecret
// - WithServer
// - WithBasePath
// - WithRouter
// - WithLogger
// - WithConfigStorage
// - WithEncryptionSecret
// - WithManagers
// - WithAPP
func Init(options ...Options) *API {
	api := &API{
		BaseRoutes: make(router),
	}

	for _, option := range options {
		option(api)
	}

	api.logger.WithField("basePath", api.basePath).Infoln("Api configured with")

	api.BaseRoutes["ApiRoot"] = api.rootRouter.PathPrefix(api.basePath).Subrouter()
	baseRoutes := api.BaseRoutes["ApiRoot"]

	api.BaseRoutes["Services"] = baseRoutes.PathPrefix("/services").Subrouter()
	api.InitServices(api.BaseRoutes["Services"])

	var apiNames []string
	for service := range api.BaseRoutes {
		if service != "ApiRoot" {
			apiNames = append(apiNames, service)
		}
	}

	api.logger.WithField("api", strings.Join(apiNames, ",")).Infoln("Initialized api")
	return api
}
