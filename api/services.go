package api

import (
	"io"
	"net/http"
	"os"
	"proxymanager/model"

	"github.com/gorilla/mux"
)

// InitServices initializes the routes for handling proxy configurations.
// It sets up the following endpoints:
// - POST /{id:[A-Za-z0-9_-]+}: Registers a new proxy configuration.
// - GET /{id:[A-Za-z0-9_-]+}: Retrieves a proxy configuration by its ID.
//
// Parameters:
//   - contacts: The router to which the endpoints are added.
//
// Errors:
//   - Sets an application error in the context if there is an issue creating the file, reading the request body,
//     writing the encrypted data to the file, or writing the response.
//
// Success:
//   - Writes a success message to the response if the proxy configuration is registered successfully.
func (api *API) InitServices(contacts *mux.Router) {
	contacts.Handle("/{id:[A-Za-z0-9_-]+}", api.APIHandler(registerProxyConfig)).Methods("POST")
	contacts.Handle("/{id:[A-Za-z0-9_-]+}", api.APISessionRequired(getProxyConfigByID)).Methods("GET")
}

// registerProxyConfig handles the registration of a proxy configuration.
// It reads the request body, encrypts it, and writes it to a file specified by the request parameters.
// If any error occurs during the process, it sets the appropriate error in the context.
//
// Parameters:
//   - c: The context containing application-specific information.
//   - w: The HTTP response writer to send the response.
//   - r: The HTTP request containing the proxy configuration data.
//
// Errors:
//   - Sets an application error in the context if there is an issue creating the file, reading the request body,
//     writing the encrypted data to the file, or writing the response.
//
// Success:
//   - Writes a success message to the response if the proxy configuration is registered successfully.
func registerProxyConfig(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	f, fileError := os.Create(c.App.ConfigStorageLocation + "/" + params["id"])
	if fileError != nil {
		c.Err = model.NewAppError("sidecar.registerProxyConfig", "sidecar.registerProxyConfig.app_error", nil, "id="+fileError.Error(), http.StatusBadRequest)
		return
	}

	body, readerError := io.ReadAll(r.Body)
	if readerError != nil {
		c.Err = model.NewAppError(
			"sidecar.registerProxyConfig",
			"sidecar.registerProxyConfig.read.body.app_error",
			nil,
			"id="+readerError.Error(),
			http.StatusBadRequest,
		)
		return
	}

	if _, err := f.Write(c.App.Encrypt(body)); err != nil {
		c.Err = model.NewAppError(
			"sidecar.registerProxyConfig",
			"sidecar.registerProxyConfig.write.file.app_error",
			nil,
			"id="+err.Error(),
			http.StatusBadRequest,
		)
		return
	}
	defer func() {
		f.Close()
	}()

	if _, err := w.Write([]byte("Successfully registered")); err != nil {
		c.Err = model.NewAppError(
			"sidecar.getProxyConfigByID",
			"response.write.app_error",
			nil,
			"id="+err.Error(),
			http.StatusBadRequest,
		)
		return
	}
}

// getProxyConfigByID retrieves the proxy configuration by its ID.
// It expects the following parameters:
// - c: The context which contains application-specific information.
// - w: The HTTP response writer to send the response.
// - r: The HTTP request containing the necessary headers and parameters.
//
// The function performs the following steps:
// 1. Extracts the parameters from the request URL and headers.
// 2. Retrieves the client registry from the application context.
// 3. Validates the app ID and app key using the client registry.
// 4. Reads the proxy configuration file from the storage location based on the provided ID.
// 5. Decrypts the content and writes it to the response.
//
// If any error occurs during these steps, it sets the error in the context and returns without writing the response.
func getProxyConfigByID(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	appKey := r.Header.Get("APP_KEY")
	appID := r.Header.Get("APP_ID")

	registry, err := c.App.GetClientRegistry()
	if err != nil {
		c.Err = err
		return
	}

	err = registry.Validate(appID, appKey)
	if err != nil {
		c.Err = err
		return
	}

	content, fileError := os.ReadFile(c.App.ConfigStorageLocation + "/" + params["id"])
	if fileError != nil {
		c.Err = model.NewAppError("sidecar.getProxyConfigByID", "sidecar.getProxyConfigByID.app_error", nil, "id="+fileError.Error(), http.StatusBadRequest)
		return
	}

	if _, err := w.Write(c.App.Decrypt(content)); err != nil {
		c.Err = model.NewAppError("sidecar.getProxyConfigByID", "response.write.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		return
	}
}
