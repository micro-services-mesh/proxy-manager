package api

import (
	"log"
	"net/http"
	"proxymanager/app"
	"proxymanager/identity/clients"
	"proxymanager/model"
	"proxymanager/utils"
)

// handler represents a structure that holds the necessary components and configurations
// for handling HTTP requests within the proxy manager API.
//
// Fields:
// - app: A reference to the main application instance.
// - handleFunc: A function that processes the HTTP request and writes the response.
// - requireSession: A boolean indicating whether a valid session is required to handle the request.
// - trustRequester: A boolean indicating whether the requester is trusted.
// - requireMfa: A boolean indicating whether multi-factor authentication is required.
type handler struct {
	app            *app.App
	handleFunc     func(*Context, http.ResponseWriter, *http.Request)
	requireSession bool
	trustRequester bool
	requireMfa     bool
}

// Context represents the context of a request in the proxy manager API.
// It contains the application instance, translation function, error information,
// request ID, IP address, and request path.
//
// Fields:
// - App: A reference to the main application instance.
// - T: A translation function that translates error messages.
// - Err: An application error that occurred during the request.
// - RequestID: A unique identifier for the request.
// - IPAddress: The IP address of the requester.
// -	Path: The path of the request.
//
// Example:
//
//	c := &Context{
//		App:       h.app,
//		T:         utils.T,
//		RequestID: model.NewID(),
//		IPAddress: utils.GetIPAddress(r),
type Context struct {
	App       *app.App
	T         model.TranslateFunc
	Err       *model.AppError
	RequestID string
	IPAddress string
	Path      string
}

// APIHandler wraps a given handler function with additional context and returns an http.Handler.
// The returned handler includes the application context and does not require a session, trust the requester, or require MFA.
//
// Parameters:
//
//	h - A handler function that takes a Context, http.ResponseWriter, and *http.Request.
//
// Returns:
//
//	An http.Handler that wraps the provided handler function with additional context.
func (api *API) APIHandler(h func(*Context, http.ResponseWriter, *http.Request)) http.Handler {
	return &handler{
		app:            api.App,
		handleFunc:     h,
		requireSession: false,
		trustRequester: false,
		requireMfa:     false,
	}
}

// APISessionRequired is a middleware function that wraps an HTTP handler function
// to enforce that a valid API session is required for the request. It ensures that
// the requester has an active session, and optionally enforces multi-factor
// authentication (MFA) if required.
//
// Parameters:
//
//	h - The HTTP handler function to be wrapped.
//
// Returns:
//
//	An http.Handler that enforces session requirements before invoking the provided handler function.
func (api *API) APISessionRequired(h func(*Context, http.ResponseWriter, *http.Request)) http.Handler {
	return &handler{
		app:            api.App,
		handleFunc:     h,
		requireSession: true,
		trustRequester: false,
		requireMfa:     true,
	}
}

// ServeHTTP handles incoming HTTP requests, sets up the context, and processes the request.
// It performs the following steps:
// 1. Initializes a new Context with the application instance, request ID, IP address, and request path.
// 2. Retrieves the App ID from the request headers, defaulting to "UNKNOWN" if not provided.
// 3. If session validation is required, it retrieves the API key from the headers and validates the client using the client registry.
// 4. Sets the request ID and content type headers in the response.
// 5. If no error occurred during validation, it calls the handler function to process the request.
// 6. If an error occurred, it translates the error, logs it, and writes the error response with the appropriate status code.
//
// Parameters:
// - w: The HTTP response writer.
// - r: The HTTP request.
//
// Example:
//
//	h := &handler{
//		app:            api.App,
//		handleFunc:     h,
//		requireSession: false,
//		trustRequester: false,
//		requireMfa:     false,
//	}
//	h.ServeHTTP(w, r)
func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := &Context{
		App:       h.app,
		T:         utils.T,
		RequestID: model.NewID(),
		IPAddress: utils.GetIPAddress(r),
		Path:      r.URL.Path,
	}

	headers := r.Header
	appID := headers.Get(model.HeaderAppID)

	if appID == "" {
		appID = "UNKNOWN"
	}

	if h.requireSession {
		apiKey := headers.Get(model.HeaderAppKey)
		if appID != "" || apiKey != "" {
			// Validate client.
			var clientRegistry *clients.Registry
			clientRegistry, c.Err = c.App.GetClientRegistry()
			if c.Err == nil {
				c.Err = clientRegistry.Validate(appID, apiKey)
			}
		} else {
			c.Err = model.NewAppError("context.ServeHTTP", "context.ServeHttp.missing_app_details", nil, "Missing app secret", http.StatusBadRequest)
		}
	}

	w.Header().Set(model.HeaderRequestID, c.RequestID)
	w.Header().Set("Content-Type", "application/json")

	if c.Err == nil {
		h.handleFunc(c, w, r)
		return
	}

	c.Err.Translate(c.T)
	c.Err.RequestID = c.RequestID
	c.Err.Where = r.URL.Path

	c.App.Logger.WithField("APP_ID", appID).Errorln(c.Err.ToJSON())

	w.WriteHeader(c.Err.StatusCode)
	if _, err := w.Write([]byte(c.Err.ToJSON())); err != nil {
		log.Println("Unable to write the response")
	}
}
