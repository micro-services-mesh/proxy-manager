package api

import (
	"proxymanager/app"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// Options is a type alias for a function that takes a pointer to an API struct.
// It is used to configure or modify the API instance.
type Options func(a *API)

// WithAPP sets the provided app.App instance to the API.
// It returns an Options function that assigns the given app.App to the API's App field.
//
// Parameters:
//
//	app - a pointer to an app.App instance to be set in the API.
//
// Returns:
//
//	An Options function that sets the provided app.App instance to the API.
func WithAPP(app *app.App) Options {
	return func(a *API) {
		a.App = app
	}
}

// WithBasePath sets the base path for the API.
// It takes a string parameter `basePath` which specifies the base path to be set.
// It returns an Options function that modifies the basePath field of the API struct.
//
// Parameters:
//
//	basePath - a string representing the base path to be set.
//
// Returns:
//
//	An Options function that sets the provided base path to the API.
//
// Example usage:
//
//	api := NewAPI(WithBasePath("/api/v1"))
func WithBasePath(basePath string) Options {
	return func(a *API) {
		a.basePath = basePath
	}
}

// WithRouter sets the root router for the API.
// It takes a *mux.Router as an argument and returns an Options function
// that assigns the provided router to the API's rootRouter field.
//
// Parameters:
//
//	root - a pointer to a mux.Router instance to be set in the API.
//
// Returns:
//
//	An Options function that sets the provided router to the API.
//
// Example usage:
//
//	router := mux.NewRouter()
//	api := NewAPI(WithRouter(router))
func WithRouter(root *mux.Router) Options {
	return func(a *API) {
		a.rootRouter = root
	}
}

// WithLogger sets the logger for the API.
// It takes a logrus.FieldLogger as an argument and returns an Options function
// that assigns the provided logger to the API instance.
//
// Example usage:
//
//	logger := logrus.New()
//	api := NewAPI(WithLogger(logger))
func WithLogger(logger logrus.FieldLogger) Options {
	return func(a *API) {
		a.logger = logger
	}
}
