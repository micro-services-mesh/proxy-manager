#!/bin/bash

set -ex

BINARY_NAME=$(basename $PWD)

exec ./bin/${BINARY_NAME} serve \
  --listen=127.0.0.1:8774 \
  --log-level="debug"\
  --identifier-registration-conf="identifier-registration.yaml"\
  --signing-private-key=./examples/private-key.pem \
  --encryption-secret=./examples/encryption.key \
  --log-timestamp=true
