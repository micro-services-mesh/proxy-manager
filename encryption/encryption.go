package encryption

import (
	"fmt"

	"golang.org/x/crypto/nacl/secretbox"
)

const (
	// KeySize is the size of the keys created by GenerateKey()
	KeySize = 32
	// NonceSize is the size of the nonces created by GenerateNonce()
	NonceSize = 24
)

// Encrypt generates a random nonce and encrypts the input using nacl.secretbox
// package. We store the nonce in the first 24 bytes of the encrypted text.
//
// Parameters:
// - msg: The message to be encrypted.
// - key: A pointer to a byte array of size KeySize used as the key for encryption.
//
// Returns:
// - []byte: The encrypted message, including the nonce.
// - error: An error if the encryption fails.
//
// Example:
//
//	encrypted, err := Encrypt([]byte("Hello, World!"), key)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code encrypts the message "Hello, World!" using the provided key.
// It logs an error if the encryption fails.
func Encrypt(msg []byte, key *[KeySize]byte) ([]byte, error) {
	nonce, err := GenerateNonce()
	if err != nil {
		return nil, err
	}

	return encryptWithNonce(msg, nonce, key)
}

// encryptWithNonce encrypts a message using a given nonce and key.
// It returns the encrypted message or an error if the encryption fails.
//
// Parameters:
// - msg: The message to be encrypted.
// - nonce: A pointer to a byte array of size NonceSize used as the nonce for encryption.
// - key: A pointer to a byte array of size KeySize used as the key for encryption.
//
// Returns:
// - []byte: The encrypted message, including the nonce.
// - error: An error if the encryption fails.
func encryptWithNonce(msg []byte, nonce *[NonceSize]byte, key *[KeySize]byte) ([]byte, error) {
	encrypted := secretbox.Seal(nonce[:], msg, nonce, key)
	return encrypted, nil
}

// Decrypt extracts the nonce from the encrypted text, and attempts to decrypt
// with nacl.box.
//
// Parameters:
// - msg: The encrypted message to be decrypted.
// - key: A pointer to a byte array of size KeySize used as the key for decryption.
//
// Returns:
// - []byte: The decrypted message.
// - error: An error if the decryption fails.
//
// Example:
//
//	decrypted, err := Decrypt(encrypted, key)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code decrypts the encrypted message using the provided key.
// It logs an error if the decryption fails.
func Decrypt(msg []byte, key *[KeySize]byte) ([]byte, error) {
	if len(msg) < (NonceSize + secretbox.Overhead) {
		return nil, fmt.Errorf("wrong length of ciphertext")
	}

	var nonce [NonceSize]byte
	copy(nonce[:], msg[:NonceSize])
	decrypted, ok := secretbox.Open(nil, msg[NonceSize:], &nonce, key)
	if !ok {
		return nil, fmt.Errorf("decryption failed")
	}

	return decrypted, nil
}
