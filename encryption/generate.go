package encryption

import (
	"crypto/rand"
	"io"
)

// GenerateKey generates a new random key of size KeySize bytes.
// It returns a pointer to the generated key and an error if any occurs during the key generation process.
//
// Example:
//
//	key, err := GenerateKey()
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code generates a new random key of size KeySize bytes.
// It logs an error if any issues occur during the key generation process.
func GenerateKey() (*[KeySize]byte, error) {
	key := new([KeySize]byte)
	_, err := io.ReadFull(rand.Reader, key[:])
	if err != nil {
		return nil, err
	}

	return key, nil
}

// GenerateNonce generates a new random nonce of size NonceSize.
// It returns a pointer to the nonce and an error if the random
// number generation fails.
//
// Example:
//
//	nonce, err := GenerateNonce()
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code generates a new random nonce of size NonceSize.
// It logs an error if the random number generation fails.
func GenerateNonce() (*[NonceSize]byte, error) {
	nonce := new([NonceSize]byte)
	_, err := io.ReadFull(rand.Reader, nonce[:])
	if err != nil {
		return nil, err
	}

	return nonce, nil
}
