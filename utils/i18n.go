package utils

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
)

// locales is a map that stores localization strings for different languages.
// The keys are language codes (e.g., "en" for English, "es" for Spanish),
// and the values are the corresponding localized strings.
var locales = make(map[string]string)

// localizer is a pointer to an i18n.Localizer instance used for localization.
// It is initialized with the default language code "en" (English).
var localizer = &i18n.Localizer{}

// TODO: to manage the multiple language stuff. implement as follow
// - create bundler manager
// - create bundle for each language
// - register bundle to bindler manager
// - create get function. which return the bundle based on lang code.
var bundle = &i18n.Bundle{}

// TranslationsPreInit initializes the translation system by parsing and composing
// the language code, creating a new i18n bundle, and initializing translations
// from the specified directory. It also sets up a localizer for the given language code.
// Returns an error if any step in the initialization process fails.
func TranslationsPreInit() error {
	var code string = "ja"
	tag, err := language.Parse(code)
	if err != nil {
		// TODO: user loguras to log the error.
		log.Println("Unable to parse the language code :", code)
	}

	lang, err := language.Compose(tag)
	if err != nil {
		log.Println("Unable to compose the language : ", lang)
		return err
	}

	bundle = i18n.NewBundle(lang)

	if err := InitTranslationsWithDir("i18n"); err != nil {
		return err
	}

	localizer = i18n.NewLocalizer(bundle, code)

	return nil
}

// InitTranslationsWithDir initializes translations by loading all JSON files
// from the specified directory. It expects the directory to contain translation
// files in JSON format. Each file's name (without extension) is used as the locale key.
//
// Parameters:
//   - dir: The directory path where the translation files are located.
//
// Returns:
//   - error: An error if the directory is not found or if there is an issue loading
//     any of the translation files.
//
// Example:
//
//	err := InitTranslationsWithDir("i18n")
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code initializes translations by loading all JSON files from the "i18n" directory.
// It logs an error if the directory is not found or if there is an issue loading any of the translation files.
func InitTranslationsWithDir(dir string) error {
	i18nDirectory, found := FindDir(dir)
	if !found {
		return fmt.Errorf("unable to find i18n directory")
	}

	files, _ := os.ReadDir(i18nDirectory)
	for _, f := range files {
		if filepath.Ext(f.Name()) == ".json" {
			filename := f.Name()
			locales[strings.Split(filename, ".")[0]] = i18nDirectory + filename

			if _, err := bundle.LoadMessageFile(i18nDirectory + filename); err != nil {
				return err
			}
		}
	}

	return nil
}

// FindDir searches for the specified directory in the current and parent directories.
// It returns the absolute path of the directory and a boolean indicating whether the
// directory was found.
//
// Parameters:
//   - dir: The directory name to search for.
//
// Returns:
//   - string: The absolute path of the directory.
//   - bool: A boolean indicating whether the directory was found.
//
// Example:
//
//	dir, found := FindDir("i18n")
//	if found {
//		fmt.Println("Directory found at:", dir)
//	}
//
// The above code searches for the "i18n" directory in the current and parent directories.
// It prints the absolute path of the directory if found.
func FindDir(dir string) (string, bool) {
	fileName := "."
	found := false

	if _, err := os.Stat("./" + dir + "/"); err == nil {
		fileName, _ = filepath.Abs("./" + dir + "/")
		found = true
	} else if _, err := os.Stat("../" + dir + "/"); err == nil {
		fileName, _ = filepath.Abs("../" + dir + "/")
		found = true
	} else if _, err := os.Stat("../../" + dir + "/"); err == nil {
		fileName, _ = filepath.Abs("../../" + dir + "/")
		found = true
	}

	return fileName + "/", found
}

// T translates a message based on the provided translationID and optional arguments.
// It uses the localizer to fetch the localized message. If the translationID is invalid,
// it logs the error and returns an empty string.
//
// Parameters:
//   - translationID: The ID of the message to be translated.
//   - args: Optional arguments to be used as template data for the message.
//
// Returns:
//   - The localized message as a string. If an error occurs, an empty string is returned.
//
// Example:
//
//	msg := T("app.error", map[string]interface{}{"param1": "value1"})
//	fmt.Println(msg)
//
// The above code translates the message with the ID "app.error" using the provided template data.
// It then prints the localized message to the console.
func T(translationID string, args ...interface{}) string {
	localizeCfg := &i18n.LocalizeConfig{
		MessageID: translationID,
		DefaultMessage: &i18n.Message{
			ID: translationID,
		},
	}

	if len(args) > 0 {
		localizeCfg.TemplateData = args[0]
	}

	msg, err := localizer.Localize(localizeCfg)
	if err != nil {
		log.Println(err)
		log.Println("Invalid messageID:", translationID)
		return ""
	}
	return msg
}

// GetIPAddress return the remote IP address.
//
// Parameters:
//   - r: The http.Request object.
//
// Returns:
//   - string: The remote IP address.
//
// Example:
//
//	ip := GetIPAddress(r)
//	fmt.Println(ip)
//
// The above code retrieves the remote IP address from the http.Request object.
// It then prints the IP address to the console.
func GetIPAddress(r *http.Request) string {
	return r.RemoteAddr
}
