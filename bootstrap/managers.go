package bootstrap

import (
	"context"
	"fmt"
	identityClients "proxymanager/identity/clients"
	"proxymanager/managers"
)

// newManagers initializes and returns a new instance of managers.Managers.
// It sets up the identifier client registry manager and adds it to the managers instance.
//
// Parameters:
//   - ctx: The context for managing request-scoped values, cancellation, and timeouts.
//   - bs: A pointer to the bootstrap configuration containing necessary settings.
//
// Returns:
//   - A pointer to an instance of managers.Managers.
//   - An error if the client registry manager could not be created.
//
// Example:
//
//	mgrs, err := newManagers(ctx, bs)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code initializes a new instance of managers.Managers and adds the client registry manager to it.
// It logs an error if the client registry manager could not be created.
func newManagers(ctx context.Context, bs *bootstrap) (*managers.Managers, error) {
	logger := bs.cfg.Logger
	var err error
	mgrs := managers.New()

	// Identifier client registry manager.
	clients, err := identityClients.NewRegistry(ctx, bs.identifierRegistrationConf, logger)
	if err != nil {
		return nil, fmt.Errorf("failed to create client registry: %v", err)
	}

	mgrs.Set("clients", clients)

	return mgrs, err
}
