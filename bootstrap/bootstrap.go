package bootstrap

import (
	"proxymanager/api"
	"proxymanager/app"
	"proxymanager/config"
	"proxymanager/encryption"
	"proxymanager/managers"

	"context"
	"fmt"
	"os"
	"path/filepath"
	"proxymanager/server"

	"github.com/sirupsen/logrus"
	"stash.kopano.io/kgol/rndm"
)

// Config represents the configuration settings for the proxy manager bootstrap.
// It includes various paths, keys, and settings required for initialization.
//
// Fields:
//   - BasePath: The base path for the proxy manager.
//   - Listen: The address and port on which the proxy manager should listen.
//   - ConfigStoragePath: The path to the configuration storage.
//   - SigningPrivateKeyFiles: A list of file paths to the signing private keys.
//   - EncryptionSecretFile: The file path to the encryption secret.
//   - SigningMethod: The method used for signing.
//   - IdentifierRegistrationConf: The configuration for identifier registration.
//   - Logger: The logger used for logging within the proxy manager.
type Config struct {
	BasePath                   string
	Listen                     string
	ConfigStoragePath          string
	SigningPrivateKeyFiles     []string
	EncryptionSecretFile       string
	SigningMethod              string
	IdentifierRegistrationConf string
	Logger                     logrus.FieldLogger
}

// bootstrap is a struct that holds configuration and state information
// necessary for initializing and managing the proxy manager.
//
// Fields:
// - encryptionSecret: A byte slice containing the encryption secret used for secure operations.
// - identifierRegistrationConf: A string representing the configuration for identifier registration.
// - cfg: A pointer to the global configuration object.
// - managers: A pointer to the managers object that handles various management tasks.
//
// Example:
//
//	b := &bootstrap{
//		encryptionSecret:           []byte("secret"),
//		identifierRegistrationConf: "config.json",
//		cfg:                        config,
//		managers:                   managers,
//	}
//
// The above code creates a new bootstrap instance with the specified configuration and state information.
type bootstrap struct {
	encryptionSecret           []byte
	identifierRegistrationConf string

	cfg      *config.Config
	managers *managers.Managers
}

// Config returns the configuration settings for the bootstrap instance.
// It provides access to the configuration object which contains various
// settings and parameters required for the application to run.
//
// Returns:	- A pointer to the configuration object.
//
// Example:
//
//	cfg := b.Config()
//	fmt.Println(cfg.BasePath)
//
// The above code retrieves the configuration object from the bootstrap instance
// and prints the base path of the proxy manager.
func (b *bootstrap) Config() *config.Config {
	return b.cfg
}

// Boot initializes and sets up the bootstrap process.
//
// It takes the following parameters:
// - ctx: The context for controlling cancellation and deadlines.
// - cfg: The configuration for the bootstrap process.
// - serverCfg: The server configuration.
//
// It returns an error if the initialization or setup fails.
//
// Example:
//
//	err := Boot(context.Background(), cfg, serverCfg)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code initializes and sets up the bootstrap process with the provided
// configuration and server settings. It logs an error if the process fails.
func Boot(ctx context.Context, cfg *Config, serverCfg *config.Config) error {
	bs := &bootstrap{
		cfg: serverCfg,
	}

	if err := bs.initialize(cfg); err != nil {
		return err
	}

	if err := bs.setup(ctx, cfg); err != nil {
		return err
	}

	return nil
}

// initialize sets up the bootstrap process by loading configuration files,
// creating necessary directories, and initializing encryption secrets.
//
// Parameters:
//   - cfg: A pointer to the Config struct containing configuration settings.
//
// Returns:
//   - error: An error object if any issues occur during initialization, otherwise nil.
//
// The function performs the following steps:
//  1. Checks if the configuration storage directory exists, and creates it if it doesn't.
//  2. Loads the encryption secret from a file if specified, or generates a random secret if not.
//  3. Validates the size of the encryption secret.
//  4. Loads the identifier registration configuration file if specified, and checks its accessibility.
//
// Example:
//
//	err := initialize(cfg)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code initializes the bootstrap process with the provided configuration settings.
// It logs an error if any issues occur during initialization.
func (b *bootstrap) initialize(cfg *Config) error {
	logger := b.cfg.Logger
	var err error

	cfgLocation := cfg.ConfigStoragePath
	_, err = os.Stat(cfgLocation)
	if err != nil && os.IsNotExist(err) {
		if err = os.Mkdir(cfgLocation, os.ModePerm); err != nil {
			return fmt.Errorf("Unable to create the directory to store proxy configuration: %v", err)
		}
	}

	logger.WithField("config_storage", cfgLocation).Infoln("loading all proxy configs from folder")

	encryptionSecretFn := cfg.EncryptionSecretFile

	if encryptionSecretFn != "" {
		logger.WithField("file", encryptionSecretFn).Infoln("loading encryption secret from file")
		b.encryptionSecret, err = os.ReadFile(encryptionSecretFn)
		if err != nil {
			return fmt.Errorf("failed to load encryption secret from file: %v", err)
		}
		if len(b.encryptionSecret) != encryption.KeySize {
			return fmt.Errorf("invalid encryption secret size - must be %d bytes", encryption.KeySize)
		}
	} else {
		logger.Warnf("missing --encryption-secret parameter, using random encyption secret with %d bytes", encryption.KeySize)
		b.encryptionSecret = rndm.GenerateRandomBytes(encryption.KeySize)
	}

	b.identifierRegistrationConf = cfg.IdentifierRegistrationConf
	if b.identifierRegistrationConf != "" {
		b.identifierRegistrationConf, _ = filepath.Abs(b.identifierRegistrationConf)
		if _, errStat := os.Stat(b.identifierRegistrationConf); errStat != nil {
			return fmt.Errorf("identifier-registration-conf file not found or unable to access: %v", errStat)
		}
		logger.WithField("identifier-registration-conf", b.identifierRegistrationConf).Infoln("Identifier registration config from")
	} else {
		logger.WithField("identifier-registration-conf", b.identifierRegistrationConf).Warnln("Identifier registration config file not found or unable to access")
	}

	return nil
}

// setup initializes the bootstrap process by setting up managers, creating the application instance,
// initializing the API, and starting the server.
//
// Parameters:
//   - ctx: The context for managing the lifecycle of the setup process.
//   - cfg: The configuration settings required for the setup.
//
// Returns:
//   - error: An error if any step in the setup process fails, otherwise nil.
//
// The function performs the following steps:
//  1. Initializes the managers required for the application.
//  2. Creates a new application instance with the provided configuration settings.
//  3. Initializes the API with the application instance and other settings.
//  4. Starts the server using the application instance.
//
// Example:
//
//	err := setup(ctx, cfg)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code sets up the bootstrap process with the provided configuration settings.
// It logs an error if any step in the setup process fails.
func (b *bootstrap) setup(ctx context.Context, cfg *Config) error {
	var err error
	b.managers, err = newManagers(ctx, b)
	if err != nil {
		return err
	}

	app := app.NewApp(
		app.WithManagers(b.managers),
		app.WithEncryptionSecret(b.encryptionSecret),
		app.WithBasePath(cfg.BasePath),
		app.WithLogger(b.cfg.Logger),
		app.WithConfigStorage(cfg.ConfigStoragePath),
		app.WithServer(server.NewServer(b.cfg.Logger, cfg.Listen)),
	).(*app.App)

	api.Init(
		api.WithAPP(app),
		api.WithBasePath(cfg.BasePath),
		api.WithRouter(app.GetRouter()),
		api.WithLogger(b.cfg.Logger),
	)

	if err = app.StartServer(ctx); err != nil {
		return fmt.Errorf("Unable to start the server: %v", err)
	}
	return nil
}
