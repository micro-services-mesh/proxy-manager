package config

import (
	"net"
	"net/http"

	"github.com/sirupsen/logrus"
)

// Config represents the configuration settings for the proxy manager.
// It includes various settings required for initialization.
//
// Fields:
//   - ListenAddr: The address and port on which the proxy manager should listen.
//   - Logger: The logger used for logging within the proxy manager.
//   - HTTPTransport: The HTTP transport used for making requests.
//   - TrustedProxyIPs: A list of trusted proxy IP addresses.
//   - TrustedProxyNets: A list of trusted proxy IP networks.
//
// Example:
//
//	c := &Config{
//		ListenAddr:       "",
//		Logger:           logrus.New(),
//		HTTPTransport:    http.DefaultTransport,
//		TrustedProxyIPs:  []net.IP{net.ParseIP("
//		TrustedProxyNets: []*net.IPNet{net.IPNet{
//			IP:   net.ParseIP(""),
//			Mask: net.CIDRMask(24, 32),
//		}},
//	}
//
// The above code creates a new configuration instance with the provided settings.
// It sets the listen address, logger, HTTP transport, trusted proxy IP addresses, and trusted proxy IP networks.
type Config struct {
	ListenAddr       string
	Logger           logrus.FieldLogger
	HTTPTransport    http.RoundTripper
	TrustedProxyIPs  []*net.IP
	TrustedProxyNets []*net.IPNet
}
