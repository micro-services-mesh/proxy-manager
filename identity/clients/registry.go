package clients

import (
	"context"
	"errors"
	"net/http"
	"os"
	"proxymanager/model"
	"sync"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

// Registry implements the registry for registered clients.
// It stores client registration data and provides methods for registering
// and retrieving client data.
//
// Fields:
//   - mutex: A mutex used to synchronize access to the registry.
//   - clients: A map of client IDs to client registration data.
//   - logger: The logger used for logging debug and warning messages.
//
// Example:
//
//	registry := &Registry{
//		clients: make(map[string]*ClientRegistration),
//		logger:  logrus.New(),
//	}
//
// The above code creates a new Registry instance with an empty client map and a new logger.
// The registry can be used to register and retrieve client data.
type Registry struct {
	mutex   sync.RWMutex
	clients map[string]*ClientRegistration
	logger  logrus.FieldLogger
}

// NewRegistry creates a new Registry instance by reading client registration
// data from a specified YAML configuration file. It initializes the registry
// with the provided logger and registers each client found in the configuration.
//
// Parameters:
//   - ctx: The context for the registry creation process.
//   - registrationConfFilepath: The file path to the YAML configuration file
//     containing client registration data. If the file path is empty, no clients
//     will be registered.
//   - logger: The logger used for logging debug and warning messages.
//
// Returns:
//   - *Registry: A pointer to the newly created Registry instance.
//   - error: An error if the configuration file could not be read or parsed,
//     or if any client registration fails.
//
// Example:
//
//	registry, err := NewRegistry(context.Background(), "clients.yaml", logger)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code creates a new Registry instance by reading client registration
// data from the "clients.yaml" configuration file. It initializes the registry
// with the provided logger and registers each client found in the configuration.
func NewRegistry(ctx context.Context, registrationConfFilepath string, logger logrus.FieldLogger) (*Registry, error) {
	registryData := &RegistryData{}
	if registrationConfFilepath != "" {
		logger.Debugf("parsing identifier registration conf from %v", registrationConfFilepath)
		registryFile, err := os.ReadFile(registrationConfFilepath)
		if err != nil {
			return nil, err
		}

		err = yaml.Unmarshal(registryFile, registryData)
		if err != nil {
			return nil, err
		}
	}

	r := &Registry{
		clients: make(map[string]*ClientRegistration),
		logger:  logger,
	}

	for _, client := range registryData.Clients {
		registerErr := r.Register(client)
		fields := logrus.Fields{
			"client_id":          client.ID,
			"with_client_secret": client.AppKey != "",
			"client_name":        client.Name,
		}

		if registerErr != nil {
			logger.WithError(registerErr).WithFields(fields).Warnln("skipped registration of invalid client")
			continue
		}
		logger.WithFields(fields).Debugln("registered client")
	}
	return r, nil
}

// Register validates the provided client registration and adds the client
// to the accociated registry if valid. Returns error otherwise.
//
// Parameters:
//   - client: The client registration data to be added to the registry.
//
// Returns:
//   - error: An error if the client registration data is invalid.
//
// Example:
//
//	client := &ClientRegistration{
//		ID:     "client1",
//		Name:   "Client 1",
//		AppKey: "appKey1",
//	}
//	err := registry.Register(client)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code registers a new client with the provided client registration data.
// It logs an error if the client registration data is invalid.
func (r *Registry) Register(client *ClientRegistration) error {
	if client.ID == "" {
		return errors.New("invalid client_id")
	}

	if client.AppKey == "" {
		return errors.New("invalid app key")
	}

	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.clients[client.ID] = client

	return nil
}

// Get returns the registered clients registration for the provided client ID.
// Returns true if the client is found, false otherwise.
//
// Parameters:
//   - clientID: The client ID to look up in the registry.
//
// Returns:
//   - *ClientRegistration: The client registration data for the provided client ID.
//   - bool: True if the client is found, false otherwise.
//
// Example:
//
//	client, ok := registry.Get("client1")
//	if ok {
//		log.Println(client)
//	}
//
// The above code retrieves the client registration data for the client with the ID "client1".
// It logs the client data if the client is found.
func (r *Registry) Get(clientID string) (*ClientRegistration, bool) {
	// Lookup client registration.
	r.mutex.RLock()
	registration, ok := r.clients[clientID]
	r.mutex.RUnlock()
	if ok {
		return registration, true
	}

	return nil, false
}

// Validate checks if the provided client registration data complies to the
// provided parameters and returns error when it does not.
//
// Parameters:
//   - appID: The client ID to validate.
//   - appKey: The application key to validate.
//
// Returns:
//   - *model.AppError: An error if the client registration data is invalid.
//
// Example:
//
//	err := registry.Validate("client1",
//		"appKey1")
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code validates the client registration data for the client with the ID "client1"
// and the application key "appKey1". It logs an error if the client registration data is invalid.
func (r *Registry) Validate(appID, appKey string) *model.AppError {
	client, ok := r.Get(appID)
	if !ok {
		return model.NewAppError("identity.client.Validate", "identity.client.not.found", nil, "Client not found", http.StatusBadRequest)
	}

	if client.AppKey != appKey {
		return model.NewAppError("identity.client.Validate", "identity.client.app.key.invalid", nil, "Invalid application key", http.StatusBadRequest)
	}
	return nil
}
