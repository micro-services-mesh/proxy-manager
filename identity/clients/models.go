package clients

// RegistryData is the base structur of our client registry configuration file.
// It contains a list of clients that are allowed to connect to the proxy.
//
// Example:
//
//	clients:
//		- id: "client1"
//		  name: "Client 1"
//		  appKey: "appKey1"
//		- id: "client2"
//		  name: "Client 2"
//		  appKey: "appKey2"
//
// The above example shows a configuration file with two clients.
// Each client has an ID, name, and app key.
type RegistryData struct {
	Clients []*ClientRegistration `yaml:"clients,flow"`
}

// ClientRegistration defines a client with its properties.
// It contains an ID, name, and app key.
//
// Example:
//
//	client1:
//		id: "client1"
//		name: "Client 1"
//		appKey: "appKey1"
//
// The above example shows a client with an ID, name, and app key.
//
// Fields:
// - ID: The unique identifier for the client.
// - Name: The name of the client.
// - AppKey: The application key used for authentication.
type ClientRegistration struct {
	ID     string `yaml:"id" json:"-"`
	Name   string `yaml:"name" json:"name,omitempty"`
	AppKey string `yaml:"appKey" json:"appKey,omitempty"`
}
