package model

// Headers contains the header constants used in the application.
// These constants are used to set and retrieve headers from HTTP requests and responses.
// They are used to maintain consistency and avoid hardcoding strings.
const (
	HeaderRequestID        = "X-Request-ID"
	HeaderVersionID        = "X-Version-ID"
	HeaderClusterID        = "X-Cluster-ID"
	HeaderEtagServer       = "ETag"
	HeaderEtagClient       = "If-None-Match"
	HeaderForwarded        = "X-Forwarded-For"
	HeaderRealIP           = "X-Real-IP"
	HeaderForwardedProto   = "X-Forwarded-Proto"
	HeaderToken            = "token"
	HeaderBearer           = "BEARER"
	HeaderAuth             = "Authorization"
	HeaderRequestedWith    = "X-Requested-With"
	HeaderRequestedWithXML = "XMLHttpRequest"
	HeaderAppID            = "APP_ID"
	HeaderAppKey           = "APP_KEY"
	Status                 = "status"
	StatusOK               = "OK"
	StatusFail             = "FAIL"
	StatusRemove           = "REMOVE"
	ServiceName            = "SERVICE_NAME"
	ServiceVersion         = "SERVICE_VERSION"
	ServiceID              = "SERVICE_ID"
)
