package model

import (
	"bytes"
	"encoding/base32"
	"encoding/json"

	"github.com/pborman/uuid"
	"github.com/sirupsen/logrus"
)

// encoding is a base32 encoding scheme using a custom alphabet.
// The alphabet used is "ybndrfg8ejkmcpqxot1uwisza345h769".
var encoding = base32.NewEncoding("ybndrfg8ejkmcpqxot1uwisza345h769")

// AppError represents a structured error with detailed information.
// It includes fields for user-facing messages, internal debugging details,
// HTTP status codes, and additional context about where the error occurred.
//
// Fields:
//   - ID: A unique identifier for the error.
//   - Message: The user-facing message to display.
//   - DetailedError: The internal error message for debugging.
//   - RequestID: The unique identifier for the request.
//   - StatusCode: The HTTP status code for the error.
//   - Where: The function where the error occurred.
//   - IsOAuth: A boolean indicating whether the error is OAuth-specific.
//   - params: A map of additional parameters for the error.
//
// Example:
//
//	err := &model.AppError{
//		ID:            "app.error",
//		Message:       "An error occurred.",
//		DetailedError: "An error occurred while processing the request.",
//		Request
//		StatusCode:    http.StatusInternalServerError,
//		Where:         "app.NewApp",
//		IsOAuth:       false,
//		params:        map[string]interface{}{"param1": "value1", "param2": "value2"},
//	}
type AppError struct {
	ID            string `json:"id"`
	Message       string `json:"message"`               // Message to be display to the end user without debugging information
	DetailedError string `json:"detailed_error"`        // Internal error string to help the developer
	RequestID     string `json:"request_id,omitempty"`  // The RequestId that's also set in the header
	StatusCode    int    `json:"status_code,omitempty"` // The http status code
	Where         string `json:"-"`                     // The function where it happened in the form of Struct.Func
	IsOAuth       bool   `json:"is_oauth,omitempty"`    // Whether the error is OAuth specific
	params        map[string]interface{}
}

// NewID is a globally unique identifier.  It is a [A-Z0-9] string 26
// characters long.  It is a UUID version 4 Guid that is zbased32 encoded
// with the padding stripped off.
func NewID() string {
	var b bytes.Buffer
	encoder := base32.NewEncoder(encoding, &b)
	if _, err := encoder.Write(uuid.NewRandom()); err != nil {
		logrus.Errorln("Unable to encode uuid.")
	}
	encoder.Close()
	b.Truncate(26) // removes the '==' padding
	return b.String()
}

// TranslateFunc is a function that translates a message based on a translation ID and optional arguments.
// It returns the translated message as a string.
//
// Parameters:
//   - translationID: A string representing the translation ID.
//   - args: A variadic list of arguments to be used in the translation.
//
// Returns:
//   - string: The translated message.
//
// Example:
//
//	translation := func(translationID string, args ...interface{}) string {
//		return fmt.Sprintf("Translation for %s with args %v", translationID, args)
//	}
//
//	message := translation("app.error", "arg1", "arg2")
//
// The above code defines a translation function that returns a formatted string with the translation ID and arguments.
// It then calls the translation function with a translation ID and arguments to get the translated message.
type TranslateFunc func(translationID string, args ...interface{}) string

// NewAppError creates a new AppError instance with the provided parameters.
// It returns a pointer to the newly created AppError instance.
//
// Parameters:
//   - where: The function where the error occurred.
//   - ID: A unique identifier for the error.
//   - params: A map of additional parameters for the error.
//   - details: The internal error message for debugging.
//   - status: The HTTP status code for the error.
//
// Returns:
//   - *AppError: A pointer to the newly created AppError instance.
//
// Example:
//
//	err := NewAppError("app.NewApp", "app.error", map[string]interface{}{"param1": "value1"}, "An error occurred.", http.StatusInternalServerError)
//
// The above code creates a new AppError instance with the provided parameters.
// It then returns a pointer to the newly created AppError instance.
func NewAppError(where string, ID string, params map[string]interface{}, details string, status int) *AppError {
	return &AppError{
		ID:            ID,
		params:        params,
		Message:       ID,
		Where:         where,
		DetailedError: details,
		StatusCode:    status,
		IsOAuth:       false,
	}
}

// Translate translates the error message using the provided TranslateFunc.
// If the error has parameters, they will be passed to the TranslateFunc.
//
// Parameters:
//   - T: A function that takes an error ID and optional parameters, and returns a translated message.
//
// Example usage:
//
//	err := &AppError{ID: "error_id", params: []interface{}{"param1", "param2"}}
//	err.Translate(myTranslateFunc)
//
// The above code translates the error message using the provided TranslateFunc.
// If the error has parameters, they will be passed to the TranslateFunc.
func (er *AppError) Translate(T TranslateFunc) {
	if er.params == nil {
		er.Message = T(er.ID)
	} else {
		er.Message = T(er.ID, er.params)
	}
}

// ToJSON converts the AppError instance to a JSON string.
// It returns the JSON string representation of the AppError instance.
//
// Returns:
//   - string: The JSON string representation of the AppError instance.
//
// Example:
//
//	err := &AppError{ID: "error_id", Message: "An error occurred."}
//	json := err.ToJSON()
//
// The above code converts the AppError instance to a JSON string.
// It then returns the JSON string representation of the AppError instance.
func (er *AppError) ToJSON() string {
	b, err := json.MarshalIndent(er, "", "    ")
	if err != nil {
		return ""
	}

	return string(b)
}
