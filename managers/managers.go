package managers

// ServiceUsesManagers is an interface for service which register to managers.
// It contains a single method, RegisterManagers, which takes a pointer to Managers
// and returns an error if the registration fails.
//
// Example:
//
//	type MyService struct {}
//
//	func (s *MyService) RegisterManagers(mgrs *Managers) error {
//		mgrs.Set("myManager", &MyManager{})
//		return nil
//	}
//
// Parameters:
//   - mgrs: A pointer to the Managers instance.
//
// Returns:
//
//	An error if the registration fails.
//
// Example usage:
//
//	s := &MyService{}
//	err := s.RegisterManagers(mgrs)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code registers a manager with the provided name to the Managers instance.
// It logs an error if the registration fails.
type ServiceUsesManagers interface {
	RegisterManagers(mgrs *Managers) error
}

// Managers is a registry for named managers.
// It contains a map of managers with their names as keys.
//
// Example:
//
//	mgrs := managers.New()
//	mgrs.Set("myManager", &MyManager{})
//
// The above code creates a new Managers instance and adds a manager with the name "myManager" to it.
type Managers struct {
	registry map[string]interface{}
}

// New creates a new Managers instance with an empty manager map.
//
// Example:
//
//	mgrs := managers.New()
//
// The above code creates a new Managers instance with an empty manager map.
func New() *Managers {
	return &Managers{
		registry: make(map[string]interface{}),
	}
}

// Set adds the provided manager with the provided name to the accociated
// Managers.
//
// Parameters:
//   - name: The name of the manager.
//   - manager: The manager to be added.
//
// Returns:
//
//	Nothing.
//
// Example usage:
//
//	mgrs.Set("myManager", &MyManager{})
//
// The above code adds a manager with the name "myManager" to the Managers instance.
func (m *Managers) Set(name string, manager interface{}) {
	m.registry[name] = manager
}

// Get returns the manager identified by the given name from the accociated
// managers.
//
// Parameters:
//   - name: The name of the manager.
//
// Returns:
//   - interface{}: The manager identified by the given name.
//   - bool: A boolean value indicating whether the manager was found.
//
// Example usage:
//
//	manager, ok := mgrs.Get("myManager")
//	if ok {
//		log.Println(manager)
//	}
//
// The above code retrieves the manager with the name "myManager" from the Managers instance.
// It logs the manager if it is found.
func (m *Managers) Get(name string) (interface{}, bool) {
	manager, ok := m.registry[name]

	return manager, ok
}
