package app

import (
	"context"
	"net/http"
	"proxymanager/encryption"
	"proxymanager/identity/clients"
	"proxymanager/managers"
	"proxymanager/model"
	"proxymanager/server"
	"proxymanager/utils"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// Impl defines the interface for starting a server.
// It contains a single method, StartServer, which takes a context
// and returns an error if the server fails to start.
//
// Example:
//
//	app := NewApp()
//	err := app.StartServer(context.Background())
//	if err != nil {
//		log.Fatal(err)
//	}
//
// Parameters:
//
//	ctx - a context.Context instance that is used to manage the lifecycle of the server.
//
// Returns:
//
//	An error if the server fails to start.
//	Otherwise, nil is returned.
//
// Example usage:
//
//	app := NewApp()
//	err := app.StartServer(context.Background())
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code creates a new App instance and starts the server using the StartServer method.
type Impl interface {
	StartServer(ctx context.Context) error
}

// App represents the main structure for the proxy manager application.
// It contains references to the server instance, base path, configuration storage location,
// encryption secret, managers, and a logger for logging purposes.
//
// Fields:
// - srv: A reference to the server instance.
// - basePath: The base path for the application.
// - ConfigStorageLocation: The location of the configuration storage.
// - encryptionSecret: The secret key used for encryption.
// - managers: A reference to the managers instance.
// - Logger: A logger for logging purposes.
//
// Example:
//
//	app := &App{
//		srv:                   server,
//		basePath:              "/api/v1",
//		ConfigStorageLocation: "/etc/proxymanager/config",
//		encryptionSecret:      []byte("
//		managers:              managers,
//		Logger:                logrus.New(),
//	}
//
// The above code creates a new App instance with the provided server, base path, configuration storage location,
// encryption secret, managers, and logger.
//
// The App struct is used to manage the main components of the proxy manager application,
// such as the server, configuration storage, encryption, managers, and logging.
// It provides methods for starting the server, encrypting and decrypting data, and accessing the client registry.
type App struct {
	srv                   *server.Server
	basePath              string
	ConfigStorageLocation string
	encryptionSecret      []byte
	managers              *managers.Managers
	Logger                logrus.FieldLogger
}

// NewApp func creates a new App instance with the provided options.
// It initializes the localization and returns a pointer to the App instance.
//
// Parameters:
//
//	options - A variadic list of Option functions to configure the App instance.
//
// Returns:
//
//	A pointer to the initialized App instance.
//
// Example:
//
//	app := NewApp(WithBasePath("/api/v1"), WithLogger(logger))
//
// The above code creates a new App instance with the provided base path and logger.
// The App instance is then returned for further configuration and use.
//
// The NewApp function is used to create a new App instance with the provided options.
// It initializes the localization and returns a pointer to the App instance.
func NewApp(optios ...Option) Impl {
	return newApp(optios...)
}

// newApp creates a new instance of App and applies the provided options to it.
// It initializes the localization translations and logs an error if the initialization fails.
//
// Parameters:
//
//	options - A variadic list of Option functions to configure the App instance.
//
// Returns:
//
//	*App - A pointer to the newly created App instance.
//
// Example:
//
//	a := newApp(WithBasePath("/api/v1"), WithLogger(logger))
//
// The newApp function creates a new instance of App and applies the provided options to it.
// It initializes the localization translations and logs an error if the initialization fails.
func newApp(options ...Option) *App {
	a := &App{}
	for _, option := range options {
		option(a)
	}

	if err := utils.TranslationsPreInit(); err != nil {
		a.Logger.Errorln("Unable to initialize the localization.")
	}

	return a
}

// GetRouter returns the router instance associated with the App.
// It provides access to the underlying mux.Router used by the server.
//
// Returns:
//
//	*mux.Router - A reference to the router instance.
//
// Example:
//
//	router := app.GetRouter()
//
// The above code retrieves the router instance associated with the App and assigns it to the router variable.
//
// The GetRouter method is used to retrieve the router instance associated with the App.
// It provides access to the underlying mux.Router used by the server.
func (a *App) GetRouter() *mux.Router {
	return a.srv.Router
}

// StartServer starts the server associated with the App instance.
// It takes a context as a parameter and returns an error if the server fails to start.
//
// Parameters:
//
//	ctx - A context.Context instance that is used to manage the lifecycle of the server.
//
// Returns:
//
//	error - An error if the server fails to start.
//	Otherwise, nil is returned.
//
// Example:
//
//	err := app.StartServer(context.Background())
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code starts the server associated with the App instance using the StartServer method.
//
// The StartServer method is used to start the server associated with the App instance.
// It takes a context as a parameter and returns an error if the server fails to start.
func (a *App) StartServer(ctx context.Context) error {
	return a.srv.StartServer(ctx)
}

// Encrypt func the plain text to encrypted text
// using the encryption secret key.
//
// Parameters:
//
//	plainText - A byte slice containing the plain text to be encrypted.
//
// Returns:
//
//	[]byte - A byte slice containing the encrypted text.
//
// Example:
//
//	encryptedText := app.Encrypt([]byte("Hello, World!"))
//
// The above code encrypts the plain text "Hello, World!" using the encryption secret key.
//
// The Encrypt method encrypts the provided plain text using the encryption secret key.
// It returns a byte slice containing the encrypted text.	If an error occurs during encryption, nil is returned.
func (a *App) Encrypt(plainText []byte) []byte {
	encryptedText, err := encryption.Encrypt(plainText, (*[32]byte)(a.encryptionSecret))
	if err != nil {
		return nil
	}
	return encryptedText
}

// Decrypt func decrypts the encrypted text to plain text
// using the encryption secret key.
//
// Parameters:
//
//	encryptedText - A byte slice containing the encrypted text to be decrypted.
//
// Returns:
//
//	[]byte - A byte slice containing the decrypted text.
//
// Example:
//
//	decryptedText := app.Decrypt([]byte("encrypted text"))
//
// The above code decrypts the encrypted text using the encryption secret key.
//
// The Decrypt method decrypts the provided encrypted text using the encryption secret key.
// It returns a byte slice containing the decrypted text. If an error occurs during decryption, nil is returned.
func (a *App) Decrypt(plainText []byte) []byte {
	decryptedText, err := encryption.Decrypt(plainText, (*[32]byte)(a.encryptionSecret))
	if err != nil {
		return nil
	}
	return decryptedText
}

// GetClientRegistry returns the client registry associated with the App instance.
// It provides access to the client registry used to manage client authentication.
//
// Returns:
//
//	*clients.Registry - A reference to the client registry instance.
//	*model.AppError - An error if the client registry is not found.
//
// Example:
//
//	clientRegistry, err := app.GetClientRegistry()
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code retrieves the client registry associated with the App instance and assigns it to the clientRegistry variable.
//
// The GetClientRegistry method is used to retrieve the client registry associated with the App instance.
// It provides access to the client registry used to manage client authentication.
func (a *App) GetClientRegistry() (*clients.Registry, *model.AppError) {
	clientRegistry, ok := a.managers.Get("clients")
	if !ok {
		return nil, model.NewAppError("app.GetClient", "app.client.not.found", nil, "Client not found", http.StatusBadRequest)
	}

	return clientRegistry.(*clients.Registry), nil
}
