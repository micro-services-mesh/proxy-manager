package app

import (
	"proxymanager/managers"
	"proxymanager/server"

	"github.com/sirupsen/logrus"
)

// Option represents a function that modifies the configuration or behavior of an App instance.
type Option func(a *App)

// WithBasePath sets the base path for the application.
// It returns an Option that modifies the App instance to use the provided base path.
//
// Parameters:
//   - basePath: The base path to be set for the application.
//
// Returns:
//   - Option: A function that sets the base path of the App instance.
//
// Example:
//
//	app := NewApp(WithBasePath("/api/v1"))
//
// The above code creates a new App instance with the provided base path.
// The App instance is then returned for further configuration and use.
//
// The WithBasePath function is used to set the base path for the application.
func WithBasePath(basePath string) Option {
	return func(a *App) {
		a.basePath = basePath
	}
}

// WithServer sets the server for the App.
// It takes a pointer to a server.Server and returns an Option.
// The returned Option is a function that sets the srv field of the App to the provided server.
//
// Parameters:
//   - server: A pointer to a server.Server instance to be set in the App.
//
// Returns:
//   - Option: A function that sets the provided server to the App.
//
// Example:
//
//	app := NewApp(WithServer(server))
//
// The above code creates a new App instance with the provided server.
// The App instance is then returned for further configuration and use.
//
// The WithServer function is used to set the server for the App.
func WithServer(server *server.Server) Option {
	return func(a *App) {
		a.srv = server
	}
}

// WithLogger sets the logger for the App.
// It takes a logrus.FieldLogger as an argument and returns an Option.
// The logger will be used by the App for logging purposes.
//
// Parameters:
//   - logger: A logrus.FieldLogger instance to be set in the App.
//
// Returns:
//   - Option: A function that sets the provided logger to the App.
//
// Example usage:
//
//	logger := logrus.New()
//	app := NewApp(WithLogger(logger))
func WithLogger(logger logrus.FieldLogger) Option {
	return func(a *App) {
		a.Logger = logger
	}
}

// WithConfigStorage sets the configuration storage location for the App.
// It takes a string parameter 'storage' which specifies the location of the configuration storage.
// The function returns an Option which is a function that modifies the App instance.
//
// Parameters:
//   - storage: A string representing the location of the configuration storage.
//
// Returns:
//   - Option: A function that sets the provided configuration storage location to the App.
//
// Example usage:
//
//	app := NewApp(WithConfigStorage("/etc/proxymanager/config"))
//
// The above code creates a new App instance with the provided configuration storage location.
// The App instance is then returned for further configuration and use.
func WithConfigStorage(storage string) Option {
	return func(a *App) {
		a.ConfigStorageLocation = storage
	}
}

// WithEncryptionSecret sets the encryption secret for the App.
//
// Parameters:
//
//	encryptionSecret ([]byte): The encryption secret to be used.
//
// Returns:
//
//	Option: A function that sets the encryption secret in the App.
//
// Example usage:
//
//	app := NewApp(WithEncryptionSecret([]byte("secret")))
//
// The above code creates a new App instance with the provided encryption secret.
// The App instance is then returned for further configuration and use.
func WithEncryptionSecret(encryptionSecret []byte) Option {
	return func(a *App) {
		a.encryptionSecret = encryptionSecret
	}
}

// WithManagers sets the Managers instance for the App.
//
// Parameters:
// - managers: A pointer to a Managers instance.
//
// Returns:
// - An Option function that sets the Managers instance for the App.
//
// Example:
//
//	app := NewApp(WithManagers(managers))
//
// The above code creates a new App instance with the provided Managers instance.
// The App instance is then returned for further configuration and use.
func WithManagers(managers *managers.Managers) Option {
	return func(a *App) {
		a.managers = managers
	}
}
