/*
Copyright © 2022 Snehal Dangroshiya <snehaldangroshiya@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"context"
	"fmt"
	"os"
	"proxymanager/bootstrap"
	"proxymanager/config"
	"proxymanager/encryption"

	"github.com/spf13/cobra"
)

// defaultListenAddr is the default address and port where the proxy manager server listens for incoming requests.
const (
	// defaultListenAddr is the default address and port where the proxy manager server listens for incoming requests.
	defaultListenAddr = "127.0.0.1:8774"

	// defaultBasePath is the default base path for the proxy manager service.
	basePath = "/api/v1"

	// defaultConfigStoragePath is the default path to the configuration storage.
	defaultConfigStoragePath = ".data"
)

// bootstrapConfig is a pointer to the global configuration object.
// It is used to store the configuration settings for the proxy manager bootstrap.
//
// Fields:
//   - BasePath: The base path for the proxy manager.
//   - Listen: The address and port on which the proxy manager should listen.
//   - ConfigStoragePath: The path to the configuration storage.
//   - SigningPrivateKeyFiles: A list of file paths to the signing private keys.
//   - EncryptionSecretFile: The file path to the encryption secret.
//   - SigningMethod: The method used for signing.
//   - IdentifierRegistrationConf: The configuration for identifier registration.
//   - Logger: The logger used for logging within the proxy manager.
//
// Example:
//
//	b := &bootstrap{
//		BasePath:                   "/api/v1",
//		Listen:                     "",
//		ConfigStoragePath:          ".data",
//		SigningPrivateKeyFiles:     []string{"private.key"},
//		Encryption
//		SigningMethod:              "PS256",
//		IdentifierRegistrationConf: "config.json",
//		Logger:                     logrus.New(),
//	}
//
// The above code creates a new bootstrap instance with the provided configuration settings.
// It sets the base path, listen address, configuration storage path, signing private key files,
// encryption secret file, signing method, identifier registration configuration, and logger.
var bootstrapConfig = &bootstrap.Config{}

// serveCmd represents the serve command which starts the proxy manager.
// It uses the cobra.Command struct to define the command's usage and description.
// The Run function executes the serve function and handles any errors that occur.
//
// Example:
//
//	var serveCmd = &cobra.Command{
//		Use:
//		Short:
//		Run:
//	}
//
// The above code creates a new cobra.Command instance with the specified usage, short description, and run function.
// The serve command is used to start the proxy manager service.
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Serve proxy manager",
	Run: func(cmd *cobra.Command, args []string) {
		if err := serve(cmd, args); err != nil {
			fmt.Printf("Error: %v \n\n", err)
			os.Exit(1)
		}
	},
}

// init initializes the serve command and its flags. It adds the serveCmd to the rootCmd and sets up
// various configuration options for the proxy manager service, including listen address, API base path,
// config storage location, identifier registration configuration, encryption secret file, signing private key files,
// signing method, log timestamp, and log level.
//
// Example:
//
//	init() {
//		rootCmd.AddCommand(serveCmd)
//
//		cfg := bootstrapConfig
//		serveCmd.Flags().StringVar(&cfg.Listen, "listen", getEnv("CADDY_MGR_LISTENER", defaultListenAddr), fmt.Sprintf("TCP listen address (default \"%s\").", "8774"))
//		serveCmd.Flags().StringVar(&cfg.BasePath, "api_base", getEnv("CADDY_MGR_BASE_API", basePath), "Base api path for the caddy manager service.")
//		serveCmd.Flags().StringVar(&cfg.ConfigStoragePath, "config_storage", getEnv("CADDY_MGR_CONFIG_PATH", defaultConfigStoragePath), "Caddy config storage location")
//		serveCmd.Flags().StringVar(&cfg.IdentifierRegistrationConf, "identifier-registration-conf", "", "Path to a identifier-registration.yaml configuration file")
//		serveCmd.Flags().StringVar(&cfg.EncryptionSecretFile, "encryption-secret", os.Getenv("CADDY_MGR_ENCRYPTION_SECRET"), fmt.Sprintf("Full path to a file containing a %d bytes secret key", encryption.KeySize))
//		serveCmd.Flags().StringArrayVar(&cfg.SigningPrivateKeyFiles, "signing-private-key", listEnvArg("CADDY_MGR_SIGNING_PRIVATE_KEY"), "Full path to PEM encoded private key file (must match the --signing-method algorithm)")
//		serveCmd.Flags().StringVar(&cfg.SigningMethod, "signing-method", "PS256", "JWT default signing method")
//		serveCmd.Flags().Bool("log-timestamp", true, "Prefix each log line with timestamp")
//		serveCmd.Flags().String("log-level", "info", "Log level (one of panic, fatal, error, warn, info or debug)")
//	}
//
// The above code initializes the serve command and its flags. It adds the serveCmd to the rootCmd and sets up
// various configuration options for the proxy manager service, including listen address, API base path,
// config storage location, identifier registration configuration, encryption secret file, signing private key files,
// signing method, log timestamp, and log level.
func init() {
	rootCmd.AddCommand(serveCmd)

	cfg := bootstrapConfig
	serveCmd.Flags().StringVar(&cfg.Listen, "listen", getEnv("CADDY_MGR_LISTENER", defaultListenAddr), fmt.Sprintf("TCP listen address (default \"%s\").", "8774"))
	serveCmd.Flags().StringVar(&cfg.BasePath, "api_base", getEnv("CADDY_MGR_BASE_API", basePath), "Base api path for the caddy manager service.")
	serveCmd.Flags().StringVar(&cfg.ConfigStoragePath, "config_storage", getEnv("CADDY_MGR_CONFIG_PATH", defaultConfigStoragePath), "Caddy config storage location")
	serveCmd.Flags().StringVar(&cfg.IdentifierRegistrationConf, "identifier-registration-conf", "", "Path to a identifier-registration.yaml configuration file")
	serveCmd.Flags().StringVar(&cfg.EncryptionSecretFile, "encryption-secret", os.Getenv("CADDY_MGR_ENCRYPTION_SECRET"), fmt.Sprintf("Full path to a file containing a %d bytes secret key", encryption.KeySize))
	serveCmd.Flags().StringArrayVar(&cfg.SigningPrivateKeyFiles, "signing-private-key", listEnvArg("CADDY_MGR_SIGNING_PRIVATE_KEY"), "Full path to PEM encoded private key file (must match the --signing-method algorithm)")
	serveCmd.Flags().StringVar(&cfg.SigningMethod, "signing-method", "PS256", "JWT default signing method")
	serveCmd.Flags().Bool("log-timestamp", true, "Prefix each log line with timestamp")
	serveCmd.Flags().String("log-level", "info", "Log level (one of panic, fatal, error, warn, info or debug)")

}

// serve initializes the logger and starts the bootstrap process.
//
// It retrieves the log timestamp and log level flags from the command,
// creates a new logger with the specified settings, and then calls the
// bootstrap.Boot function with the provided context and configuration.
//
// Parameters:
//   - cmd: The cobra command that triggered this function.
//   - args: The arguments passed to the command.
//
// Returns:
//   - error: An error if the logger creation fails or if the bootstrap process encounters an issue.
//
// Example:
//
//	err := serve(cmd, args)
//	if err != nil {
//		log.Fatal(err)
//	}
//
// The above code initializes the logger and starts the bootstrap process.
// It logs a fatal error if the logger creation fails or if the bootstrap process encounters an issue.
func serve(cmd *cobra.Command, args []string) error {
	ctx := context.Background()
	logTimestamp, _ := cmd.Flags().GetBool("log-timestamp")
	logLevel, _ := cmd.Flags().GetString("log-level")

	logger, err := newLogger(!logTimestamp, logLevel)
	if err != nil {
		return fmt.Errorf("failed to create logger: %v", err)
	}
	logger.Infoln("serve start")

	return bootstrap.Boot(ctx, bootstrapConfig, &config.Config{
		Logger: logger,
	})
}
