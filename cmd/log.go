package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
)

// newLogger creates a new instance of logrus.FieldLogger with the specified log level and timestamp settings.
// Parameters:
//   - disableTimestamp: A boolean indicating whether to disable timestamps in the log output.
//   - logLevelString: A string representing the desired log level (e.g., "debug", "info", "warn", "error").
//
// Returns:
//   - logrus.FieldLogger: A configured logger instance.
//   - error: An error if the log level string is invalid.
//
// Example:
//
//	logger, err := newLogger(false, "debug")
//	if err != nil {
//	  log.Fatal(err)
//	}
//	logger.Debug("This is a debug message")
//	logger.Info("This is an info message")
//	logger.Warn("This is a warning message")
//	logger.Error("This is an error message")
//
// The above code creates a new logger instance with timestamps enabled and log level set to "debug".
// It then logs messages at different levels using the logger instance.
func newLogger(disableTimestamp bool, logLevelString string) (logrus.FieldLogger, error) {
	logLevel, err := logrus.ParseLevel(logLevelString)
	if err != nil {
		return nil, err
	}

	return &logrus.Logger{
		Out: os.Stderr,
		Formatter: &logrus.TextFormatter{
			DisableTimestamp: disableTimestamp,
			TimestampFormat:  "d",
			ForceColors:      true,
		},
		Level: logLevel,
	}, nil
}
