package cmd

import (
	"os"
	"strings"
)

// getEnv retrieves the value of the environment variable named by the key `name`.
// If the variable is not present in the environment, it returns the default value `def`.
//
// Parameters:
//   - name: The name of the environment variable to retrieve.
//   - def: The default value to return if the environment variable is not set.
//
// Returns:
//
//	The value of the environment variable if it is set, otherwise the default value `def`.
//
// Example:
//
//	value := getEnv("ENV_VAR", "default_value")
//
// The above code retrieves the value of the environment variable "ENV_VAR".
// If the environment variable is not set, it returns the default value "default_value".
func getEnv(name string, def string) string {
	v := os.Getenv(name)
	if v == "" {
		return def
	}

	return v
}

// listEnvArg takes an environment variable name as input, retrieves its value,
// splits the value by spaces, trims any leading or trailing whitespace from each
// resulting substring, and returns a slice of non-empty substrings.
//
// Parameters:
//   - name: The name of the environment variable to retrieve and process.
//
// Returns:
//   - A slice of strings containing the non-empty, trimmed substrings obtained
//     from splitting the environment variable's value by spaces.
//
// Example:
//
//	list := listEnvArg("ENV_VAR")
//
// The above code retrieves the value of the environment variable "ENV_VAR",
// splits the value by spaces, trims any leading or trailing whitespace from each
// resulting substring, and returns a slice of non-empty substrings.
func listEnvArg(name string) []string {
	list := make([]string, 0)
	for _, keyFn := range strings.Split(os.Getenv(name), " ") {
		keyFn = strings.TrimSpace(keyFn)
		if keyFn != "" {
			list = append(list, keyFn)
		}
	}

	return list
}
