# proxy-manager

proxy manager which inject the proxy information into every service


## TODO
---
- Create deb package for the product release. Make this steps to automate https://www.makeuseof.com/create-deb-packages-debian-ubuntu/
### Update `rules` file with following lines.
```
override_dh_usrlocal:
	mkdir -p debian/proxymanager/usr/local/bin/
	cp ./proxy-manager debian/proxymanager/usr/local/bin/proxymanager

override_dh_installdocs:
	mkdir -p debian/proxymanager/usr/share/doc/proxymanager
	cp README.md debian/proxymanager/usr/share/doc/proxymanager
	cp LICENSE debian/proxymanager/usr/share/doc/proxymanager
	cp 3rdparty-LICENSES.md debian/proxymanager/usr/share/doc/proxymanager
```