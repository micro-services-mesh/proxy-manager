PACKAGE  = proxymanager
PACKAGE_NAME = $(shell basename $(PACKAGE))

# Tools 
GO      ?= go
GOFMT   ?= gofmt
GOLINT  ?= golangci-lint
GOLINT_ARGS?=--timeout=5m --out-format code-climate --issues-exit-code 0 | tee golangci-lint-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
GOCLEAN=$(GO) clean
GOTEST=$(GO) test
BINARY_NAME?=$(shell hostname)
BINARY_SERVICE=$(BINARY_NAME)_service
APP=app


#Variables
ARGS    ?=
PWD     := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
DATE    ?= $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2>/dev/null | sed 's/^v//' || \
			cat $(CURDIR)/.version 2> /dev/null || echo 0.0.0-unreleased)
PKGS     = $(or $(PKG),$(shell $(GO) list -mod=readonly ./... | grep -v "^$(PACKAGE)/vendor/"))
TESTPKGS = $(shell $(GO) list -mod=readonly -f '{{ if or .TestGoFiles .XTestGoFiles }}{{ .ImportPath }}{{ end }}' $(PKGS) 2>/dev/null)
CMDS     = $(PWD)

# Cgo
CGO_ENABLED ?= 0

# Go modules

GO111MODULE ?= on

# Variables

export CGO_ENABLED GO111MODULE
unexport GOPATH

# Build

LDFLAGS  ?= -s -w
ASMFLAGS ?=
GCFLAGS  ?=

.PHONY: all
all: fmt vendor | $(CMDS)

.PHONY: $(CMDS)
$(CMDS):vendor ;$(info building $@ ...) @
	$(GO) build \
		-tags release \
		-trimpath \
		-buildmode=exe \
		-asmflags '$(ASMFLAGS)' \
		-gcflags '$(GCFLAGS)' \
		-ldflags '$(LDFLAGS) -buildid=reproducible/$(VERSION) -X $(PACKAGE)/version.Version=$(VERSION) -X $(PACKAGE)/version.BuildDate=$(DATE) -extldflags -static' \
		-o bin/$(notdir $@) $@

# Helpers

.PHONY: lint
lint: vendor ; $(info running $(GOLINT) ...)	@
	$(GOLINT) run $(GOLINT_ARGS)

.PHONY: lint-checkstyle
lint-checkstyle: vendor ; $(info running $(GOLINT) checkstyle ...)     @
	@mkdir -p test
	$(GOLINT) run $(GOLINT_ARGS) --out-format checkstyle --issues-exit-code 0 > test/tests.lint.xml

.PHONY: fmt
fmt: ; $(info running gofmt ...)	@
	@ret=0 && for d in $$($(GO) list -mod=readonly -f '{{.Dir}}' ./... | grep -v /vendor/); do \
		$(GOFMT) -l -w $$d/*.go || ret=$$? ; \
	done ; exit $$ret

.PHONY: check
check: ; $(info checking dependencies ...) @
	@$(GO) mod verify && echo OK

# Tests
.PHONY: test
test: 
	$(GOTEST) -v ./...

# Mod

go.sum: go.mod ; $(info updating dependencies ...)
	@$(GO) mod tidy -v
	@touch $@

.PHONY: vendor
vendor: go.sum ; $(info retrieving dependencies ...)
	@$(GO) mod vendor -v
	@touch $@

# Dist

.PHONY: licenses
licenses: vendor ; $(info building licenses files ...)
	$(CURDIR)/scripts/go-license-ranger.py > $(CURDIR)/3rdparty-LICENSES.md

.PHONY: 3rdparty-LICENSES.md
3rdparty-LICENSES.md: licenses

.PHONY:dist
dist: 3rdparty-LICENSES.md ; $(info building dist tarball ...)
	@rm -rf "dist/${PACKAGE_NAME}-${VERSION}"
	@mkdir -p "dist/${PACKAGE_NAME}-${VERSION}"
	@cd dist && \
	cp -avf ../LICENSE "${PACKAGE_NAME}-${VERSION}" && \
	cp -avf ../README.md "${PACKAGE_NAME}-${VERSION}" && \
	cp -avf ../3rdparty-LICENSES.md "${PACKAGE_NAME}-${VERSION}" && \
	cp -avf ../bin/* "${PACKAGE_NAME}-${VERSION}" && \
	tar --owner=0 --group=0 -czvf ${PACKAGE_NAME}-${VERSION}.tar.gz "${PACKAGE_NAME}-${VERSION}" && \
	cd ..
	@rm -rf "dist/${PACKAGE_NAME}-${VERSION}"

# Rest

.PHONY: clean
clean: ; $(info cleaning ...)	@
	@rm -rf bin
	@rm -rf test/test.*
	@rm -rf vendor

.PHONY: version
version:
	@echo $(VERSION)
